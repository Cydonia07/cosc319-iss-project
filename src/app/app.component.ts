import { Component } from '@angular/core';

// Base component containing all other components.
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  header = 'Intermodule Ventilation System';

}
