import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleControllerComponent } from './module-controller.component';

describe('ModuleControllerComponent', () => {
  let component: ModuleControllerComponent;
  let fixture: ComponentFixture<ModuleControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
