import { Component, OnInit, Input } from '@angular/core';
import { Valve, FireSensor, ModuleDefinition } from '../../models/models';
import { Observable } from 'rxjs';

// Component representing one module. Primarily used to visually represent valve/fan state through the template.
@Component({
  selector: 'app-module-controller',
  templateUrl: './module-controller.component.html',
  styleUrls: ['./module-controller.component.css']
})
export class ModuleControllerComponent implements OnInit {
  @Input() module: ModuleDefinition;

  constructor() { }

  ngOnInit() {

  }

}
