import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ImvMasterControllerComponent } from './imv-master-controller/imv-master-controller.component';
import { ModuleControllerComponent } from './module-controller/module-controller.component';
import { ModuleDetailsComponent } from './module-details/module-details.component';

@NgModule({
  declarations: [
    AppComponent,
    ImvMasterControllerComponent,
    ModuleControllerComponent,
    ModuleDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
