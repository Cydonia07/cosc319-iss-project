import { Component, OnInit } from '@angular/core';
import { Valve, FireSensor, ModuleDefinition } from '../../models/models';
import { moduleDefaults, VALVE_OPERATION_TIME } from '../../models/defaults';

// Controller component for the visual representation of the ISS. 
// Contains controller code for selecting modules and manipulating module state via sidebar controls.
@Component({
  selector: 'app-imv-master-controller',
  templateUrl: './imv-master-controller.component.html',
  styleUrls: ['./imv-master-controller.component.css']
})

export class ImvMasterControllerComponent implements OnInit {
  selectedModule: ModuleDefinition;
  modules: ModuleDefinition[];

  constructor() { }

  ngOnInit() {
    // Deep clones the default module state and assigns it to the local array.
    this.modules = JSON.parse(JSON.stringify(moduleDefaults));
  }

  changeActiveModule(selection: number) {
    if (this.selectedModule && this.selectedModule.moduleID === selection) {
      this.selectedModule = { moduleID: 99, moduleName: "<No Selection>", hasFire: false, hasAirLeak: false };
      console.log("No active module");
    } else {
      this.selectedModule = this.modules[selection];
      console.log("Active module is now " + this.selectedModule.moduleName + " (" + this.selectedModule.moduleID + ")");
    }
  }

  simulateFire() {
    if (!this.selectedModule) {
      alert("A module needs to be selected!");
      return;
    }

    this.selectedModule.hasFire = true;
    this.closeAdjacentModulesValves();
  }


  simulateAirLeak() {
    if (!this.selectedModule) {
      alert("A module needs to be selected!")
      return;
    }

    this.selectedModule.hasAirLeak = true;
    this.closeAllModulesValves();
  }

  closeAdjacentModulesValves() {
    // Get adjacent module ids, add selected module id and close them
    let idsToClose = this.selectedModule.adjacentModules.slice();
    idsToClose.push(this.selectedModule.moduleID);
    this.closeModulesValvesById(idsToClose);
  }

  closeAllModulesValves() {
    // Get adjacent module ids, add selected module id and close them
    let idsToClose = [0, 1, 2, 3, 4, 5, 6, 7]
    idsToClose.push(this.selectedModule.moduleID);
    this.closeModulesValvesById(idsToClose);
  }

  closeModulesValvesById(ids: number[]) {
    setTimeout(() => {
      for (let id of ids) {
        this.modules[id].valves.forEach(function (valve) {
          valve.state = "Closed"
        });
      }
    }, VALVE_OPERATION_TIME);
  }

  areAllValvesClosed(module: any) {
    for (let valve of module.valves) {
      // Jump out if any open found
      if (valve.state == "Open") {
        return false;
      }
    }
    return true;
  }

  resetToDefault() {
    this.selectedModule = { moduleID: 99, moduleName: "<No Selection>", hasFire: false, hasAirLeak: false };
    this.modules = JSON.parse(JSON.stringify(moduleDefaults));
  }

}
