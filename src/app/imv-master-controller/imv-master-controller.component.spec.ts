import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImvMasterControllerComponent } from './imv-master-controller.component';

describe('ImvMasterControllerComponent', () => {
  let component: ImvMasterControllerComponent;
  let fixture: ComponentFixture<ImvMasterControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImvMasterControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImvMasterControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
