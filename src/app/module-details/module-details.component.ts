import { Component, OnInit, Input } from '@angular/core';
import { Valve, FireSensor, ModuleDefinition } from '../../models/models';
import { VALVE_OPERATION_TIME } from '../../models/defaults';

// Inner component within the sidebar containing current valve/fan state for selected module.
@Component({
  selector: 'app-module-details',
  templateUrl: './module-details.component.html',
  styleUrls: ['./module-details.component.css']
})
export class ModuleDetailsComponent implements OnInit {
  @Input() module: ModuleDefinition;

  constructor() { }

  ngOnInit() {
    // When component is initialized, provide a dummy module
    if (!this.module) {
      this.module = {
        moduleID: 99,
        moduleName: "<No Selection>",
        hasFire: false,
        hasAirLeak: false
      };
    }
  }

  toggleValve(valveID: number) {
    console.log("Toggling valve #" + valveID);

    setTimeout(() => {
      if (this.module.valves[valveID].state == "Open") {
        this.module.valves[valveID].state = "Closed";
        if (this.module.valves[valveID].fan) {
          this.module.valves[valveID].fan.isRunning = false;
        }
      }
      else {
        this.module.valves[valveID].state = "Open";
        if (this.module.valves[valveID].fan) {
          this.module.valves[valveID].fan.isRunning = true;
        }
      }
    }, VALVE_OPERATION_TIME);

  }

}
