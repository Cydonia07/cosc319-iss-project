// Time in milliseconds it takes to cycle a valve from open to closed and vice versa
export const VALVE_OPERATION_TIME: number = 2000;

// The default state of modules + valves
export const moduleDefaults = [
    {
        moduleID: 0,
        moduleName: "Zvezda",
        adjacentModules: [1],
        hasFire: false,
        valves: [
            {
                id: 0,
                state : "Open",
                fan: {
                    id: 0,
                    isRunning: true,
                    speed: 200
                }
            }
        ],
        hasAirLeak : false
    },
    {
        moduleID: 1,
        moduleName: "Zarya",
        adjacentModules: [0,2],
        hasFire: false,
        valves: [
            {
                id: 0,
                state : "Open",
                fan: {
                    id: 0,
                    isRunning: true,
                    speed: 200
                }
            },
            {
                id: 1,
                state : "Open"
            }
        ],
        hasAirLeak : false
    },
    {
        moduleID: 2,
        moduleName: "Unity",
        adjacentModules: [1,3,4,5],
        hasFire: false,
        valves: [
            {
                id: 0,
                state : "Open",
                fan: {
                    id: 0,
                    isRunning: true,
                    speed: 200
                }
            },
            {
                id: 1,
                state : "Open",
                fan: {
                    id: 0,
                    isRunning: true,
                    speed: 200
                }
            },
            {
                id: 2,
                state : "Open"
            },
            {
                id: 3,
                state : "Closed"
            }
        ],
        hasAirLeak : false
    },
    {
        moduleID: 3,
        moduleName: "Tranquility",
        adjacentModules: [2],
        hasFire: false,
        valves: [
            {
                id: 0,
                state : "Open",
                fan: {
                    id: 0,
                    isRunning: true,
                    speed: 200
                }
            }
        ],
        hasAirLeak : false
    },
    {
        moduleID: 4,
        moduleName: "Airlock",
        adjacentModules: [2],
        hasFire: false,
        valves: [
            {
                id: 0,
                state : "Open"
            }
        ],hasAirLeak : false
    },
    {
        moduleID: 5,
        moduleName: "Destiny",
        adjacentModules: [2,6],
        hasFire: false,
        valves: [
            {
                id: 0,
                state : "Open"
            },
            {
                id: 1,
                state : "Open",
                fan: {
                    id: 0,
                    isRunning: true,
                    speed: 200
                }
            }
        ],hasAirLeak : false
    },
    {
        moduleID: 6,
        moduleName: "Node 2",
        adjacentModules: [5,7],
        hasFire: false,
        valves: [
            {
                id: 0,
                state : "Open"
            },
            {
                id: 1,
                state : "Open",
                fan: {
                    id: 0,
                    isRunning: true,
                    speed: 200
                }
            }
        ],hasAirLeak : false
    },
    {
        moduleID: 7,
        moduleName: "Columbus",
        adjacentModules: [6],
        hasFire: false,
        valves: [
            {
                id: 0,
                state : "Open"
            }
        ],hasAirLeak : false
    },
];
