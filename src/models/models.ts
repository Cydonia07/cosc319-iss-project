// This file contains JSON definitions for all types utilized.

export type Valve = {
    id: number;
    state?: string;
    fan?: Fan;
};

export type Fan = {
    id: number;
    isRunning?: boolean;
    speed?: number;
};

export type FireSensor = {
    id: number;
    temperature?: number;
    smokeDetected?: boolean;
};

// Latter 3 properties are optional until implementation
export type ModuleDefinition = {
    moduleID: number;
    moduleName: string;
    hasFire: boolean;
    hasAirLeak: boolean;
    valves?: Valve[];
    fireSensors?: FireSensor[];
    adjacentModules?: number[];
};
