import { Cosc319IssProjectPage } from './app.po';

describe('cosc319-iss-project App', () => {
  let page: Cosc319IssProjectPage;

  beforeEach(() => {
    page = new Cosc319IssProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
